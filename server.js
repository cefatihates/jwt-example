const fileStream = require('fs');
const jwt = require('jsonwebtoken');

const payload = {
    features: {
        appendDataExtensions: true,
        autoCreateDataExtension: true,
        compareFields: true,
    }
}

const privateKey = fileStream.readFileSync('./private.key');
const publicKey = fileStream.readFileSync('./public.key');

var issuer = 'https://dev.deselect.io';
var sub = 'user@email.com';
var audience = 'mc.s4.marketingcloud.com';

var signOptions = {
    issuer:  issuer,
    subject:  sub,
    audience:  audience,
    expiresIn:  "24h",
    algorithm:  "RS256"
};

var featureToken = jwt.sign(payload, privateKey, signOptions);
console.log(featureToken);

var verifyOptions = {
    issuer:  issuer,
    subject:  sub,
    audience:  audience,
    expiresIn:  "24h",
    algorithm:  ["RS256"]
};
try {
    
var featureVerified = jwt.verify(featureToken, publicKey, verifyOptions);
console.log(featureVerified);
} catch (error) {
    console.log(error);
}